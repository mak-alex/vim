#!/bin/sh
VIMHOME=~/.vim

warn() {
    echo "$1" >&2
}

die() {
    warn "$1"
    exit 1
}

[ -e "$VIMHOME/vimrc" ] && die "$VIMHOME/vimrc already exists"
[ -e "~/.vim" ] && die "~/.vim already exists"
[ -e "~/.vimrc" ] && die "~/.vimrc already exists"

git clone git@bitbucket.org:d_e_n_i_e_d/vim.git "$VIMHOME"
cd "$VIMHOME"
git submodule update --init

./install-vimrc.sh

echo "vimrc is installed successfully."
